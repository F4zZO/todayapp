"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const axios_1 = __importDefault(require("axios"));
const mongodb_1 = require("mongodb");
const PORT = process.env.PORT || 3000;
const app = express_1.default();
// Connection URL
const url = 'mongodb+srv://today:mongodb@todaycluster.5gudd.mongodb.net/TodayCluster?retryWrites=true';
const client = new mongodb_1.MongoClient(url);
let db;
let dataCollection;
connect();
async function connect() {
    await client.connect();
    console.log("Connected to db");
    db = client.db("TodayCluster");
    dataCollection = db.collection("todayData");
}
async function getData(topic, date) {
    const result = await dataCollection.findOne({ date: { $eq: date } });
    if (result && topic in result) {
        return result[topic];
    }
    return null;
}
app.get('/', getContent);
async function getContent(req, res) {
    const date = req.query.date;
    const language = req.query.language;
    const topic = req.query.topic;
    const result = await getData(topic, new Date(date));
    if (result == null) {
        res.status(404).json("No topic found for today");
        return;
    }
    const requestURL = `https://${language}.wikipedia.org/w/api.php?action=query&format=json&prop=extracts%7Cpageimages&list=&exintro=1&explaintext=1&pithumbsize=250&titles=${result}`;
    console.log(requestURL);
    const answer = await axios_1.default.get(requestURL);
    const pages = answer.data.query.pages;
    const page = pages[Object.keys(pages)[0]];
    const response = { image: page.thumbnail.source, text: page.extract, name: result };
    res.status(200).send(response);
}
app.listen(PORT, () => console.log(`Running on ${PORT} ⚡`));
