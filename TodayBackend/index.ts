import express, { Express, Request, Response } from 'express';
import axios from 'axios';
import { Collection, MongoClient } from 'mongodb';

const PORT = process.env.PORT || 3000;
const app: Express = express();


// Connection URL
const url = 'mongodb+srv://today:mongodb@todaycluster.5gudd.mongodb.net/TodayCluster?retryWrites=true';
const client = new MongoClient(url);
let db;
let dataCollection : Collection;

connect();
async function connect() {
    await client.connect();
    console.log("Connected to db");
    db = client.db("TodayCluster");
    dataCollection = db.collection("todayData");
}

async function getData(topic: string, date: Date) {
    const result = await dataCollection.findOne({ date: {$eq: date }})
    if (result && topic in result) {
        return result[topic]
    }
    return null;
}

app.get('/', getContent);

async function getContent(req: Request, res: Response) {
    const date : string = req.query.date as string;
    const language : string = req.query.language as string;
    const topic : string = req.query.topic as string;
    
    const result = await getData(topic, new Date(date));

    if (result == null) {
        res.status(404).json("No topic found for today");
        return;
    }

    const requestURL = `https://${language}.wikipedia.org/w/api.php?action=query&format=json&prop=extracts%7Cpageimages&list=&exintro=1&explaintext=1&pithumbsize=250&titles=${result}`;
    console.log(requestURL);
    const answer = await axios.get(requestURL);
    const pages = answer.data.query.pages;
    const page = pages[Object.keys(pages)[0]];
    const response: WikiResponse = { image: page.thumbnail.source, text: page.extract, name: result}
    res.status(200).send(response);
}

interface WikiResponse {
    image: string;
    text: string;
    name : string
}

app.listen(PORT, () => console.log(`Running on ${PORT} ⚡`));