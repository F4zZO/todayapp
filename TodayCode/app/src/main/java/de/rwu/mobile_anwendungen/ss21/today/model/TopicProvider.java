package de.rwu.mobile_anwendungen.ss21.today.model;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content.FrgContentEvent;
import de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content.FrgContentFilm;
import de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content.FrgContentHoliday;
import de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content.FrgContentNews;
import de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content.FrgContentPerson;
import de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content.FrgContentWord;

public class TopicProvider {

    private static List<Topic> topicList;

    public static void loadTopics(Context context) {
        List<Topic> list = new ArrayList<>();
        try (FileInputStream fileIn = context.openFileInput("Topics.txt"); ObjectInputStream in = new ObjectInputStream(fileIn)) {
            list = (List<Topic>) in.readObject();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
        }
        topicList = list;
    }

    public static void saveTopics(Context context) {
        try (FileOutputStream fileOutputStream = context.openFileOutput("Topics.txt", Context.MODE_PRIVATE); ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);) {
            objectOutputStream.writeObject(topicList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static List<Topic> getTopicList() {
        if (topicList.isEmpty()) {
            topicList.add(new Topic("Person", FrgContentPerson.class));
            topicList.add(new Topic("Event", FrgContentEvent.class));
            topicList.add(new Topic("Word", FrgContentWord.class));
            topicList.add(new Topic("News", FrgContentNews.class));
            topicList.add(new Topic("Film", FrgContentFilm.class));
            topicList.add(new Topic("Holiday", FrgContentHoliday.class));
        }
        return topicList;
    }

    public static int getSelectedCount() {
        return (int) getTopicList().stream().filter(Topic::isSelected).count();
    }

    public static List<Topic> getSelectedTopics() {
        return getTopicList().stream().filter(Topic::isSelected).collect(Collectors.toList());
    }
}
