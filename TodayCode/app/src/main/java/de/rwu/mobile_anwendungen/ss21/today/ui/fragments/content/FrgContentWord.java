package de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.json.JSONException;

import de.rwu.mobile_anwendungen.ss21.today.R;
import de.rwu.mobile_anwendungen.ss21.today.ui.logic.GetContent;

public class FrgContentWord extends Fragment {

    Context context;
    private View view;

    public FrgContentWord() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        this. context = context;
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.view = view;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*
        GetContent.get(this.context, "person", response -> {
            TextView text = this.view.findViewById(R.id.tvText);
            text.setMovementMethod(new ScrollingMovementMethod());
            try {
                text.setText(response.getString("text"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }); */
        return inflater.inflate(R.layout.frg_content_word, container, false);
    }
}