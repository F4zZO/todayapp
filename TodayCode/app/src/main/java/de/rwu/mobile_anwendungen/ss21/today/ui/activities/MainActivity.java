package de.rwu.mobile_anwendungen.ss21.today.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import de.rwu.mobile_anwendungen.ss21.today.R;
import de.rwu.mobile_anwendungen.ss21.today.model.TopicProvider;
import de.rwu.mobile_anwendungen.ss21.today.ui.logic.OnSwipeTouchListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        findViewById(android.R.id.content).getRootView().setOnTouchListener(new OnSwipeTouchListener(this) {
            public void onSwipeLeft() {
                Intent intent = new Intent(this.context, ContentActivity.class);
                startActivity(intent);
            }
            public void onSwipeRight() {
                Intent intent = new Intent(this.context, SettingsActivity.class);
                startActivity(intent);
            }
        });

        TopicProvider.loadTopics(this);
    }
}