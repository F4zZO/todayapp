package de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.rwu.mobile_anwendungen.ss21.today.R;

public class FrgContentFilm extends Fragment {


    public FrgContentFilm() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_content_film, container, false);
    }
}