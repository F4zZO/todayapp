package de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.List;

import de.rwu.mobile_anwendungen.ss21.today.R;
import de.rwu.mobile_anwendungen.ss21.today.model.Topic;
import de.rwu.mobile_anwendungen.ss21.today.model.TopicProvider;

public class FrgSlider extends Fragment {
    DemoCollectionAdapter demoCollectionAdapter;
    ViewPager2 viewPager;
    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_slider, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        createTabLayout();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.view = view;
        createTabLayout();
    }

    private void createTabLayout() {
        demoCollectionAdapter = new DemoCollectionAdapter(this);
        viewPager = view.findViewById(R.id.pager);
        viewPager.setAdapter(demoCollectionAdapter);
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        List<Topic> topic = TopicProvider.getSelectedTopics();
        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> tab.setText(topic.get(position).getName())
        ).attach();
    }
}