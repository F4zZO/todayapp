package de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;

import de.rwu.mobile_anwendungen.ss21.today.R;
import de.rwu.mobile_anwendungen.ss21.today.ui.logic.GetContent;

public class FrgContentPerson extends Fragment {

    Context context;
    private View view;

    public FrgContentPerson() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        this. context = context;
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        this.view = view;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        GetContent.get(this.context, "person", response -> {
            TextView head = this.view.findViewById(R.id.tvHeadP);
            ImageView img = this.view.findViewById(R.id.imgPicP);
            TextView text = this.view.findViewById(R.id.tvTextP);
            text.setMovementMethod(new ScrollingMovementMethod());

            try {
                head.setText(response.getString("name"));
                Picasso.get().load(response.getString("image")).into(img);
                text.setText(response.getString("text"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        return inflater.inflate(R.layout.frg_content_person, container, false);
    }
}