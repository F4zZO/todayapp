package de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import de.rwu.mobile_anwendungen.ss21.today.R;

public class FrgContentNews extends Fragment {

    public FrgContentNews() {
        // Required empty public constructor
    }

    public static FrgContentNews newInstance() {
        return new FrgContentNews();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_content_news, container, false);
    }
}