package de.rwu.mobile_anwendungen.ss21.today.model;

import java.util.ArrayList;
import java.util.List;

public class SettingsModel {

    private static final SettingsModel instance = new SettingsModel();
    private String language = "de";

    private final List<String> languages;

    private SettingsModel() {
        languages = new ArrayList<>();
        languages.add("de");
        languages.add("en");
    }

    public static SettingsModel getInstance() {
        return instance;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public List<String> getLanguages() {
        return languages;
    }
}
