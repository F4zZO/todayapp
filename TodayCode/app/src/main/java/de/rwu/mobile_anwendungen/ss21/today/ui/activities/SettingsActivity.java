package de.rwu.mobile_anwendungen.ss21.today.ui.activities;

import android.app.UiModeManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationBarView;

import java.util.Collections;
import java.util.List;

import de.rwu.mobile_anwendungen.ss21.today.R;
import de.rwu.mobile_anwendungen.ss21.today.model.SettingsModel;
import de.rwu.mobile_anwendungen.ss21.today.model.Topic;
import de.rwu.mobile_anwendungen.ss21.today.model.TopicAdapter;
import de.rwu.mobile_anwendungen.ss21.today.model.TopicProvider;
import de.rwu.mobile_anwendungen.ss21.today.ui.logic.OnSwipeTouchListener;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        findViewById(android.R.id.content).getRootView().setOnTouchListener(new OnSwipeTouchListener(this) {
            public void onSwipeLeft() {
                Intent intent = new Intent(this.context, ContentActivity.class);
                startActivity(intent);
            }
            public void onSwipeRight() {
                Intent intent = new Intent(this.context, ContentActivity.class);
                startActivity(intent);
            }
        });

        UiModeManager uiModeManager = ContextCompat.getSystemService(this, UiModeManager.class);
        SwitchCompat themeSwitch = findViewById(R.id.themeSwitch);

        //TODO: Fix changing of theme
        themeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                buttonView.setText(R.string.darkMode);
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            } else {
                buttonView.setText(R.string.lightMode);
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        });

        themeSwitch.setChecked((uiModeManager != null ? uiModeManager.getNightMode() : 0) == UiModeManager.MODE_NIGHT_YES);

        List<Topic> topics = TopicProvider.getTopicList();

        RecyclerView recyclerView = findViewById(R.id.topicList);
        RecyclerView.Adapter<TopicAdapter.TopicViewHolder> adapter = new TopicAdapter(this, topics);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {

                int start = viewHolder.getAdapterPosition();
                int end = target.getAdapterPosition();

                Collections.swap(topics, start, end);

                adapter.notifyItemMoved(start, end);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            }
        });

        itemTouchHelper.attachToRecyclerView(recyclerView);

        SettingsModel settingsModel = SettingsModel.getInstance();
        Spinner languageSpinner = findViewById(R.id.languageSpinner);
        languageSpinner.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, settingsModel.getLanguages()));

        languageSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                settingsModel.setLanguage(settingsModel.getLanguages().get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
