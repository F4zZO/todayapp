package de.rwu.mobile_anwendungen.ss21.today.model;

import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class Topic implements Serializable {

    private boolean selected;
    private final String name;

    private final Class<?extends Fragment> _class;

    public Topic(boolean selected, String name, Class<?extends Fragment> _class) {
        this.selected = selected;
        this.name = name;
        this._class = _class;
    }

    public Topic(String name, Class<?extends Fragment> _class){
        this(true, name, _class);
    }

    public String getName() {
        return name;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public Class<?extends Fragment> get_Class() { return _class; }

    @NotNull
    @Override
    public String toString() {
        return "Topic{" +
                "selected=" + selected +
                ", name='" + name + '\'' +
                '}';
    }
}

