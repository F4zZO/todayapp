package de.rwu.mobile_anwendungen.ss21.today.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.rwu.mobile_anwendungen.ss21.today.R;

public class MainFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        String dateS = DateTimeFormatter.ofPattern("dd-MM-yyyy").format(LocalDateTime.now());
        String dayS = DateTimeFormatter.ofPattern("EEEE").format(LocalDateTime.now());
        TextView day = view.findViewById(R.id.tvDay);
        TextView date = view.findViewById(R.id.tvDate);

        day.setText(dayS);
        date.setText(dateS);
        return view;
    }
}