package de.rwu.mobile_anwendungen.ss21.today.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.rwu.mobile_anwendungen.ss21.today.R;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.TopicViewHolder> {

    private final Context context;
    private final List<Topic> topics;

    public TopicAdapter(Context context, List<Topic> topics) {
        this.context = context;
        this.topics = topics;
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.topic, parent, false);
        return new TopicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicViewHolder holder, int position) {
        Topic topic = topics.get(position);
        holder.topicCheckBox.setChecked(topic.isSelected());
        holder.topicCheckBox.setText(topic.getName());
        holder.topicCheckBox.setOnClickListener(v -> {
            topic.setSelected(((CheckBox) v).isChecked());
            TopicProvider.saveTopics(context);
        });
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }

    public static class TopicViewHolder extends RecyclerView.ViewHolder {

        final CheckBox topicCheckBox;

        public TopicViewHolder(@NonNull View itemView) {
            super(itemView);
            topicCheckBox = itemView.findViewById(R.id.topicCheckBox);
        }
    }
}
