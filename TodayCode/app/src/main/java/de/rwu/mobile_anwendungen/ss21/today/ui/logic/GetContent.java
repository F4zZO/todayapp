package de.rwu.mobile_anwendungen.ss21.today.ui.logic;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.rwu.mobile_anwendungen.ss21.today.model.SettingsModel;


public class GetContent {

    public static void get(Context context, String topic, Response.Listener<JSONObject> listener){

        String date = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String lang = SettingsModel.getInstance().getLanguage();

        String url = String.format("https://today-backend.herokuapp.com/?date=%s&language=%s&topic=%s", date, lang, topic);

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                listener,
                error -> VolleyLog.d("", "Error: " + error.getMessage())
        );
        requestQueue.add(jsonObjectRequest);
    }
}
