package de.rwu.mobile_anwendungen.ss21.today.ui.fragments.content;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import de.rwu.mobile_anwendungen.ss21.today.model.TopicProvider;


public class DemoCollectionAdapter extends FragmentStateAdapter {

    public DemoCollectionAdapter(Fragment fragment) {
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        try {
            return TopicProvider.getSelectedTopics().get(position).get_Class().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new DemoObjectFragment();
    }

    @Override
    public int getItemCount() {
        return TopicProvider.getSelectedCount();
    }
}
